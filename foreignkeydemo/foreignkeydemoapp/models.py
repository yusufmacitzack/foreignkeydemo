from django.db import models

# Create your models here.
class Products(models.Model): 
    product_id = models.AutoField(primary_key=True)
    product_type = models.IntegerField()
    product_description = models.CharField(max_length=300)
    product_deliveryFeeType = models.ForeignKey("GeneralDeliveryFeeTypes", on_delete=models.CASCADE)    
    product_liveChatUse = models.BooleanField()
    product_defaultChannel = models.IntegerField()

class GeneralDeliveryFeeTypes(models.Model): 
    fee_type = models.AutoField(primary_key=True)
    fee_description_id = models.ForeignKey("OtherDeliveryFeeTypes", on_delete=models.CASCADE, default=None)    

    def __str__(self):
        return f"{self.fee_description_id}" 

class OtherDeliveryFeeTypes(models.Model): 
    OtherDeliveryFeeTypesId = models.AutoField(primary_key=True)
    fee_description = models.CharField(max_length=300)

    
    def __str__(self):
            return f"{self.fee_description}" 