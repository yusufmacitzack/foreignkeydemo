from django.shortcuts import render
from .models import * 
from django.forms import ModelForm

# Create your views here.
def index(request):

    if request.method == 'POST':
        form = ProductsForm(request.POST)

        if form.is_valid():
            form.save()
            form = ProductsForm()
            return render(request,"index.html" , {"form" : form})


        print('ÇALIŞMADI 12312313 \n    \n\n')

    form = ProductsForm()
    return render(request,"index.html" , {"form" : form})


class ProductsForm(ModelForm):

    class Meta:
        model = Products
        
        fields = [
            # 'members_id',
            # 'customer_id',
            "product_id",
            "product_type",
            "product_description",
            "product_deliveryFeeType",
            "product_liveChatUse",
            "product_defaultChannel",
        ]